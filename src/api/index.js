const axios = require('axios');
axios.defaults.adapter = require('axios/lib/adapters/http')
const getTodos = async (apiUrl) => {
	try {
		return await axios.get(apiUrl + '/todos');
	} catch (error) {
		throw new Error(error.message || error);
	}
};

const postTodos = async (apiUrl, postData) => {
	try {
		return  axios
		.post(
			apiUrl + "/todos",
		  {
			task:  postData
		  }
		)
	} catch (error) {
		throw new Error(error.message || error);
	}
};

module.exports = {
	getTodos,
	postTodos,
};
