import React from 'react';
import { getTodos, postTodos } from '../api';

let endpoint = "http://localhost:8080";

class Todo extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
          task: "",
          items: [],
          todos: {},
        };
      }
      setInputTask = (event) => {
        const { value } = event.target;
        this.setState({ task: value });
      };

      componentDidMount() {
        this.getTask();
      }
      
      onSubmit = () => {
        let { task } = this.state;
        if (task) {
          postTodos(endpoint,task)
            .then(res => {
              this.getTask();
              this.setState({
                task: ""
              });
              console.log(res);
            });
        }
      };
      getTask = () => {
        getTodos(endpoint).then(res => {          
          if (res.data) {
            this.setState({
              items: res.data.map(item => { 
                return (
                  <div > 
                    <ul className="list-group" key={item._id} >
                      <li className="list-group-item "  >
                        <div>
                          {item.task}
                        </div>
                      </li>
                    </ul> 
                  </div>
                );
              })
            });
          } 
          else {
            this.setState({
              items: []
            });
          }
        });
      };
    
      
      render() {
        return (
          <div >
            <div data-testid="todo-header">Your Tasks</div>
            <div className="row"> 
              <input 
                data-testid="input-area"
                id="input-area"
                placeholder="Add task"
                onChange={this.setInputTask}
                value={this.state.task}
              />
              { 
              <button
                data-testid='add-btn'
                onClick={this.onSubmit}>Add 
              </button> 
              }
               
            </div>
            <div className="row" id="todos-list" >
            <ul  data-testid="todos-list"  >
                {this.state.items}
            </ul> 
            </div>
          </div>
        );
      }


}
export default Todo;