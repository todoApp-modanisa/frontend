const assert = require('assert');
const { Pact } = require('@pact-foundation/pact');

const { getData, postData } = require('../../assets/mockData');
const { getTodos, postTodos } = require('../../api');

const apiUrl = 'http://127.0.0.1:8080';

describe('Consumer Tests Todo App', () => {
	const provider = new Pact({
		port: 8080,
		consumer: 'todo-app-consumer',
		provider: 'todo-app-provider',
		pactfileWriteMode: 'update',
	});

	beforeAll(() => provider.setup());

	describe('When a request is sent to Todo API', () => {
		describe('When a GET request is sent to Todo API', () => {
			beforeEach(() => {
				return provider.addInteraction({
					uponReceiving: 'All todos',
					withRequest: {
						path: '/todos',
						method: 'GET',
					},
					willRespondWith: {
						status: 200,
						body: getData,
					},
				});
			});
			it('Will recieve the list of todos', async () => {
				getTodos(apiUrl)
					.then((r) => {
						assert.ok(r.status === 200);
					})
					.catch((error) => {
						throw new Error(error.message || error);
					});
			});
		});
		describe('When a POST request is sent to Todo API', () => {
			beforeEach(() => {
				return provider.addInteraction({
					uponReceiving: 'Post a new todo',
					withRequest: {
						path: '/todos',
						method: 'POST',
						body: {task: postData},
					},
					willRespondWith: {
						status: 200,
					},
				});
			});

			it('Will recive the status code 200', async () => {
				postTodos(apiUrl, postData)
					.then((r) => {
						assert.ok(r.status === 200);
					})
					.catch((error) => {
						throw new Error(error.message || error);
					});
			});
		});
	});
	afterAll(() => provider.finalize());
});