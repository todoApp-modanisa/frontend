const postData = {
	todo: 'spor yap ',
};

const getData = {
	todos: [
		{
			_id: '1',
			todo: 'spor yap',
		},
		{
			_id: '2',
			todo: 'markete git',
		},
		{
			_id: '3',
			todo: 'okula git',		
		},
	],
};

module.exports = {
	postData,
	getData,
};
