import * as React from 'react';
import { render } from '@testing-library/react';
import Todo from '../../Components/Todo';

test('Header renders properly',async () =>{
    const wrapper=render(<Todo/>);
    const header=wrapper.getByTestId('todo-header');
    expect(header).toBeTruthy();
 })
test('Check header text',async () =>{
    const wrapper=render(<Todo/>);
    const header=wrapper.getByTestId('todo-header');
    expect(header.textContent).toBe("Your Tasks");
})

test('Textarea renders properly', async () =>{
    const wrapper=render(<Todo/>);
    const textarea=wrapper.getByTestId('input-area');
	expect(textarea).toBeTruthy();
});

test('Add button renders properly',async () =>{
    const wrapper=render(<Todo/>);
    const addButton= wrapper.getByTestId('add-btn');
    expect(addButton).toBeTruthy();
  });

test('Check add button text',async () =>{
    const wrapper=render(<Todo/>);
    const addButton= wrapper.getByTestId('add-btn');
    expect(addButton.textContent).toBe("Add");
})

test('List renders properly',async () =>{
    const wrapper=render(<Todo />);
    const list= wrapper.getByTestId('todos-list');
    expect(list).toBeTruthy();
});